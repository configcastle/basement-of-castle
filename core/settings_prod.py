# flake8: noqa

from core.settings import *

from decouple import config

SECRET_KEY = config('SECRET_KEY')

CORS_ORIGIN_WHITELIST = [
    config('FRONTED_URL'),
]

CORS_ALLOW_HEADERS = default_headers

CORS_ALLOW_METHODS = default_methods
