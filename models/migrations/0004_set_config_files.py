# Generated by Django 2.2 on 2019-12-14 00:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('models', '0003_auto_20191214_0023'),
    ]

    operations = [
        migrations.AddField(
            model_name='set',
            name='config_files',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='models.ConfigFile'),
        ),
    ]
