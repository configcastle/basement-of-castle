from django.contrib import admin

from models.models import Component, ConfigFile, Set


admin.site.register([Component, ConfigFile, Set])
