from django.contrib.postgres.fields import JSONField
from django.db import models


class Component(models.Model):
    name = models.CharField(max_length=255)
    data = JSONField(null=True)

    def __str__(self):
        return self.name


class ConfigFile(models.Model):
    available_type_of_file = (
        ('dc', 'Docker-compose'),
    )

    name = models.CharField(max_length=255)
    data_global = JSONField(null=True)
    data_services = JSONField(null=True)
    type_of_file = models.CharField(
        max_length=2,
        choices=available_type_of_file,
        default='dc'),
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)


class Set(models.Model):
    name = models.CharField(max_length=255)
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    config_files = models.ForeignKey(ConfigFile, on_delete=models.CASCADE,
                                     null=True)
