from rest_framework import serializers

from models.models import Set, ConfigFile, Component


class SetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Set
        fields = '__all__'


class ConfigFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfigFile
        fields = '__all__'


class ComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = '__all__'
