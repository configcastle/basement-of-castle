from django.urls import path

from api import views


urlpatterns = [
    path('set/<int:pk>', views.SetAPI.as_view()),
    path('set', views.SetListAPI.as_view()),
    path('component/<int:pk>', views.ComponentAPI.as_view()),
    path('component', views.ComponentListAPI.as_view()),
    path('configfile/<int:pk>', views.ConfigFileAPI.as_view()),
    path('configile', views.ConfigFileListAPI.as_view()),
]
