from rest_framework.generics import (ListCreateAPIView,
                                     RetrieveUpdateDestroyAPIView)

from api import serializer
from models.models import Set, Component, ConfigFile


class SetListAPI(ListCreateAPIView):
    queryset = Set.objects.all()
    serializer_class = serializer.SetSerializer

    def perform_create(self, serializer):
        return serializer.save()


class SetAPI(RetrieveUpdateDestroyAPIView):
    queryset = Set.objects.all()
    serializer_class = serializer.SetSerializer


class ComponentListAPI(ListCreateAPIView):
    queryset = Component.objects.all()
    serializer_class = serializer.ComponentSerializer

    def perform_create(self, serializer):
        return serializer.save()


class ComponentAPI(RetrieveUpdateDestroyAPIView):
    queryset = Component.objects.all()
    serializer_class = serializer.ComponentSerializer


class ConfigFileListAPI(ListCreateAPIView):
    queryset = ConfigFile.objects.all()
    serializer_class = serializer.ConfigFileSerializer

    def perform_create(self, serializer):
        return serializer.save()


class ConfigFileAPI(RetrieveUpdateDestroyAPIView):
    queryset = ConfigFile.objects.all()
    serializer_class = serializer.ConfigFile
